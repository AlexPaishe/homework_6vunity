﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Collections;
using System.Runtime.InteropServices;

namespace HomeWork_6v1
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputFile = @"inputNumber.txt";
            IsInputFile(inputFile);
            Console.WriteLine("Загружаю число из файла...");
            int number = ReadFile(inputFile);                           //Считываю txt файл
            int[][] result = new int[ReturnGroupCount(number)][];       //Массив массивов для хранения последовательностей чисел
            result[0] = new int[] { 1 };                                //Первая последовательность = 1
            Stopwatch sv = new Stopwatch();                             //Таймер для замера времени выполнения задачи
            int[] res = new int[ReturnGroupCount(number)];
            res[0] = 1;

            Console.WriteLine("Внимание! Число в файле = " + number);
            Console.WriteLine();
            Console.WriteLine("Выберите режим работы: \n" +
                "1 - Показать только кол-во групп\n" +
                "2 - Выполнить алгоритм подсчета групп чисел методом перебора значений\n" +
                "с последующей архивацией или сохранением в файл\n" +
                "Требуется мощный процессор. Миллиард не обработает\n" +
                "3 - Быстрый алгоритм, не требует большого объема памяти.\n" +
                "Возможно обработает Миллиард значений. Но нужен мощный процессор");
            switch (Console.ReadLine())
            {
                #region case "1": //Показать только кол-во групп
                case "1":
                    int[] range = new int[number - 2];
                    sv.Start();
                    Console.WriteLine("Групп чисел: " + ReturnGroupCount(number));
                    sv.Stop();
                    PrintTime(sv);
                    break;
                #endregion

                #region case "2": //Выполнить алгоритм подсчета групп чисел методом перебора значений
                case "2":
                    sv.Start();
                    Console.WriteLine("Формирую массив чисел от 1 до вашего числа");
                    range = new int[number - 2];
                    range = Enumerable.Range(2, number).ToArray();

                    for (int i = 1; i < result.Length; i++)
                    {
                        result[i] = ReturnList(range);             //  Находим требуемую последовательность
                        range = range.Except<int>(result[i]).ToArray();    //  Вычитаем из нашей последовательности полученную последовательность
                    }
                    sv.Stop();
                    PrintTime(sv);

                    Console.WriteLine("Производим запись результатов в файл output2.txt");
                    string file = "output2.txt";
                    SaveToFile(file, result);

                    Console.WriteLine("Сохранение в файл завершено!\n");
                    Console.WriteLine("Заархивировать полученный файл? \n" +
                        "1 - Да\n" +
                        "0 - Нет, просто выйти из программы");
                    switch (Console.ReadLine())
                    {
                        case "1":
                            Compressig(file, "archivedOutput2.zip");
                            Console.WriteLine("Файл заархивирован. Ищите его в папке с exe файлом");
                            break;
                        case "0":
                            break;
                    }
                    break;
                #endregion

                #region "3": //Решето Эратосфена
                case "3":
                    Console.WriteLine("Формирую массив чисел от 1 до вашего числа");
                    sv.Start();
                    BitArray numbers = new BitArray(number);
                    numbers[0] = true;
                    sv.Stop();
                    PrintTime(sv);
                    Console.WriteLine();
                    Console.WriteLine("Начинаю рассчет последовательностей чисел...");
                    sv = new Stopwatch();
                    sv.Start();
                    for (int i = 1; i < result.Length; i++)
                    {
                        Stopwatch s = new Stopwatch();
                        s.Start();
                        BitArray buffer = ErastofenBool(numbers);
                        numbers = ReversBool(numbers, buffer);
                        result[i] = FromBitToInt(buffer).Where(x => x >= 2).ToArray();
                        s.Stop();
                        Console.WriteLine($"Последовательность № {i} сформирована за {s.Elapsed.TotalSeconds} сек.");
                    }
                    sv.Stop();
                    PrintTime(sv);

                    Console.WriteLine("Производим запись результатов в файл output3.txt");
                    SaveToFile("output3.txt", result);
                    Console.WriteLine("Запись окончена!");
                    break;
                case "4":
                    Console.WriteLine("Формирую массив чисел от 1 до вашего числа");
                    sv.Start();
                    BitArray Numbers = new BitArray(number);
                    Numbers[0] = true;
                    sv.Stop();
                    PrintTime(sv);
                    Console.WriteLine();
                    Console.WriteLine("Начинаю рассчет последовательностей чисел...");
                    sv = new Stopwatch();
                    bool newfile = true;
                    sv.Start();
                    for (int i = 1; i < 1000; i++)
                    {
                        Stopwatch s = new Stopwatch();
                        s.Start();
                        BitArray buffer = ErastofenBool(Numbers);
                        Numbers = ReversBool(Numbers, buffer);
                        res = FromBitToInt(buffer).Where(x => x >= 2).ToArray();
                        SaveToFiles("output3.txt", res,newfile);
                        newfile = false;
                        s.Stop();
                        Console.WriteLine($"Последовательность № {i} сформирована за {s.Elapsed.TotalSeconds} сек.");
                        Console.WriteLine("Производим запись результатов в файл output3.txt");
                        Console.WriteLine("Запись окончена!");
                    }
                    sv.Stop();
                    PrintTime(sv);
                    break;
                    #endregion
            }
            Console.ReadLine();
        }

        static void SaveToFiles(string name, int[] array, bool NewFile)
        {
            string fileName = name;
            if (NewFile == true)
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                using (StreamWriter sw = new StreamWriter(new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write)))
                {
                    for (int i = 0; i < array.Length; i++)
                    {
                        sw.WriteLine(PrintArray(array));
                        sw.Flush();
                    }
                }
            }
            else if (NewFile == false)
            {
                using (StreamWriter sw = new StreamWriter(new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write)))
                {
                    for (int i = 0; i < array.Length; i++)
                    {
                        sw.WriteLine(PrintArray(array));
                        sw.Flush();
                    }
                }
            }
        }
        #region Методы выделения последовательностей

        /// <summary>
        /// Вычисляет последовательность простых чисел методом "Решето Эрастофена"
        /// </summary>
        /// <param name="list">Исходный массив битов. ненужные элементы заменябтся на true</param>
        /// <returns>Возвращает массив, где false = простые числа</returns>
        static BitArray ErastofenBool(BitArray list)
        {
            BitArray arr = new BitArray(list);
            for (int i = 2; i * 2 < arr.Length; i*=2)
            {
                if (!list[i])
                {
                    for (int j = i * 2; j < arr.Length; j += i)
                    {
                        arr[j] = true;
                    }
                }
            }
            return arr;
        }

        /// <summary>
        /// Возвращает последовательность чисел не делящихся друг на друга
        /// </summary>
        /// <param name="array">Исходный массив для выделения последовательности</param>
        /// <returns></returns>
        static int[] ReturnList(int[] array)
        {
            int[] buffArray = new int[array.Length];
            array.CopyTo(buffArray, 0);
            bool flag = true;
            for (int i = 0; i * 2 < buffArray.Length; i++)
            {
                if (buffArray[i] != 0)
                {
                    for (int j = i; j < buffArray.Length; j++)
                    {
                        if (buffArray[j] % buffArray[i] == 0 && buffArray[j] / buffArray[i] > 1)
                        {
                            buffArray[j] = 0;
                            flag = false;
                        }
                    }
                }
            }
            if (!flag) return ReturnList(buffArray);
            else return buffArray.Where(x => x != 0).ToArray();        //  Записываем последовательнсть в массив, пропуская нули
        }
        #endregion

        #region Работа с файлами

        /// <summary>
        /// Считывание числа из файла
        /// </summary>
        /// <param name="path">Полный путь к файлу</param>
        /// <returns>Возвращает число</returns>
        static int ReadFile(string path)
        {
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                return int.Parse(sr.ReadLine());
            }
        }

        /// <summary>
        /// Проверка существования файла ввода
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        static void IsInputFile(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine($"Файл данных inputNumber.txt не найден. Создаю файл по умолчанию");
                using (StreamWriter sw = new StreamWriter(new FileStream("inputNumber.txt", FileMode.Create, FileAccess.Write)))
                {
                    sw.WriteLine("1000");
                }
            }
        }

        /// <summary>
        /// Архивация файла стандартными средствами
        /// </summary>
        /// <param name="source">Исходный файл</param>
        /// <param name="output">Выходной файл архива</param>
        static void Compressig(string source, string output)
        {
            if (!File.Exists(source)) Console.WriteLine("Файл не существует! Создайте файл для архивации или проверьте правильность пути");
            else
            {
                using (FileStream fs = new FileStream(source, FileMode.Open))
                {
                    using (FileStream nf = File.Create(output))
                    {
                        using (GZipStream gs = new GZipStream(nf, CompressionMode.Compress))
                        {
                            fs.CopyTo(nf);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Сохранение результата вычисления в файл
        /// </summary>
        /// <param name="name">Путь к файлу</param>
        /// <param name="array">Массив для сохранения</param>
        static void SaveToFile(string name, int[][] array)
        {
            string fileName = name;
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            using (StreamWriter sw = new StreamWriter(new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write)))
            {
                for (int i = 0; i < array.Length; i++)
                {
                    sw.WriteLine(PrintArray(array[i]));
                    sw.Flush();
                }
            }
        }
        #endregion

        #region Вспомогательные методы обработки массивов

        /// <summary>
        /// Метод выводящий строку с замером времени выполнения алгоритма в секундах.
        /// </summary>
        /// <param name="sv">Stopwatch</param>
        static void PrintTime(Stopwatch sv)
        {
            Console.WriteLine("Время выполнения алгоритма вычисления: " + sv.Elapsed.TotalSeconds + " сек.");
        }

        /// <summary>
        /// Метод сравнения 2х списков. Заменяет в первом списке элементы нулями, содержащиеся во втором массиве
        /// </summary>
        /// <param name="list1">список, который выведется</param>
        /// <param name="list2">список для сравнения</param>
        /// <returns>Возвращает список 1, в котором элементы списка 2 заменены нулями</returns>
        static BitArray ReversBool(BitArray arr1, BitArray arr2)
        {
            BitArray result = new BitArray(arr1);
            for (int i = 0; i < result.Length; i++)
            {
                if (arr1[i] == arr2[i] && !arr1[i]) result[i] = !arr1[i];
            }
            return result;
        }

        /// <summary>
        /// Вычисляем количество групп чисел, не делящихся друг на друга (степень двойк плюс единица)
        /// </summary>
        /// <param name="number">Количество чисел в последовательности от нуля</param>
        /// <returns>Возвращает количество групп чисел, не делящихся друг на друга</returns>
        static int ReturnGroupCount(int number)
        {
            int count = 1;
            while (number > 1)
            {
                number /= 2;
                count++;
            }
            return count;
        }

        /// <summary>
        /// Преобразует одномерный массив в строку
        /// </summary>
        /// <param name="array">Одномерный массив чисел</param>
        /// <returns>Возвращает строку</returns>
        static string PrintArray(int[] array)
        {
            string result = null;
            foreach (int item in array)
            {
                result += item + " ";
            }
            return result;
        }

        /// <summary>
        /// Преобразование битового массива в int[]
        /// </summary>
        /// <param name="array">Битовый массив</param>
        /// <returns>Массив, где число равно порядковому номеру бита в исходном массиве</returns>
        static int[] FromBitToInt(BitArray array)
        {
            int[] result = new int[array.Length];
            for (int j = 1; j < array.Length; j++)
            {
                if (!array[j])
                    result[j] = j;
                else
                    result[j] = 0;
            }
            return result;
        }

        #endregion


    }
}

